一、站内信
    1）mongodb与mysql区别？
        mysql:关系型数据库，数据的结构是固定
            将磁盘的数据读到内存(buffer pool)中
        mongodb:非关系型数据库，数据结构不固定的，文档存储的
            查询效率很高，内存映射技术（Memory-mapped Files）来提供快速的读取操作，通过将磁盘上的数据文件映射到内存中，可以加快数据的访问速度
            MongoDB的数据默认存储在磁盘上，而不是存储在内存中。当数据被写入数据库时，MongoDB将其持久化到磁盘上的数据文件中。这样即使服务器关闭或重启，数据也不会丢失
    2）mongodb的增删改查语句

二、
id是主键(聚族索引)
index(name) （非聚族索引）
id          name    age
1           kate    3
2           mall    4
。。。。
100000      abc     44
