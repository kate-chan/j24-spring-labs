<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 2023/7/27
  Time: 9:23 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/taglib.jsp" %>
<html>
<head>
    <title>jb-aptech毕业设计项目</title>
    <script src="${basePath}/script/jquery-1.12.4.min.js"></script>
    <link href="../css/style.css" rel="stylesheet" type="text/css">
    <script src="../script/common.js"></script>
</head>
<body>
<div class="page_title">销售机会管理</div>
<div class="button_bar">
    <button class="common_button" onclick="help('');">帮助</button>
    <button class="common_button" onclick="to('add.html');">新建</button>
    <button class="common_button" onclick="reload();">查询</button>
</div>
<form>
    <table class="query_form_table">
        <tr>
            <th>客户名称</th>
            <td><input name="custName" id="usrName"/></td>
            <th>概要</th>
            <td><input name="title" id="title"/></td>
            <th>联系人</th>
            <td>
                <input name="linkman" id="linkman" size="20" />
            </td>
        </tr>
    </table>
</form>
<br />
<div id="chance-page-box">
    <jsp:include page="listTemplate.jsp"/>
</div>


</body>
</html>
<script type="text/javascript">
    $(function(){
        $.ajax({
            type: "GET", //请求方式 Post  get
            url: "template",//请求地址 （找得到对应的controller的方法）
            success: function(msg){//成功后的响应  msg返回的数据
                // alert( "Data Saved: " + msg );
                console.log(msg);
                $("#chance-page-box").html(msg);//替换掉id为order-data的元素的元素体  <h1><span></span> </h1>
                //3.替换分页数据
            }
        });
    })
</script>
