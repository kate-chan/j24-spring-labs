<%--
  Created by IntelliJ IDEA.
  User: mr.chan
  Date: 2020-12-29
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
          pageEncoding="UTF-8"%>
<%@include file="../common/taglib.jsp" %>
<html>
<head>
    <title>分页数据模板</title>

</head>
<body>

<table class="data_list_table">
    <tr>
        <th>编号</th>
        <th>客户名称</th>
        <th>概要</th>
        <th>联系人</th>
        <th>联系人电话</th>
        <th>创建时间</th>
        <th>操作</th>
    </tr>

    <c:forEach items="${salChancePageInfo.getList()}" var="salChance">
        <tr>

            <td class="list_data_number">${salChance.chcId}</td>
            <td class="list_data_text">${salChance.chcCustName}</td>
            <td class="list_data_ltext">${salChance.chcDesc}</td>
            <td class="list_data_text">${salChance.chcLinkman}</td>
            <td class="list_data_text">${salChance.chcTel}</td>
            <td class="list_data_text"><fmt:formatDate value="${salChance.chcCreateDate}" pattern="yyyy年MM月dd日"/> </td>
            <td class="list_data_op">
                <img onclick="to('dispatch.html');" title="指派" src="${basePath}/images/bt_linkman.gif" class="op_button" />
                <c:if test="${salChance.getChcStatus()==1}">
                    <img onclick="to('info/${salChance.getChcId()}');" title="编辑" src="${basePath}/images/bt_edit.gif" class="op_button" />
                </c:if>
                <img onclick="del('“销售机会：采购笔记本电脑意向”');" title="删除" src="${basePath}/images/bt_del.gif" class="op_button" />
            </td>
        </tr>
    </c:forEach>
    <tr>
        <th colspan="7" class="pager">
            <div class="pager">
                共${salChancePageInfo.getTotal()}条记录 每页<input value="10" size="2" />条
                第<input value="${salChancePageInfo.getPageNum()}" size="2"/>页/共${salChancePageInfo.getPages()}页
                <a href="javascript:to_page(1)">第一页</a>
                <a href="javascript:to_page(${salChancePageInfo.getPageNum()-1})">上一页</a>
                <a href="javascript:to_page(${salChancePageInfo.getPageNum()+1})">下一页</a>
                <a href="javascript:to_page(${salChancePageInfo.getPages()})">最后一页</a>
                转到<input value="1" size="2" />页
                <button width="20" onclick="reload();">GO</button>
            </div>
        </th>
    </tr>

</table>
</body>
</html>

<script type="text/javascript">

    function to_page(psize){
        //1.拼接获取到的获取表单值和分页
        var param = "psize="+psize;
        if(!($("#usrName").val()=='' && $("#title").val()=='')&&$("#linkman").val()==''){
            param+="&"+ $("form").serialize();
        }
        //2.发送异步请求
        $.ajax({
            type: "GET", //请求方式 Post  get
            url: "template",//请求地址 （找得到对应的controller的方法）
            data: param,//请求参数
            success: function(msg){//成功后的响应  msg返回的数据
                // alert( "Data Saved: " + msg );
                console.log(msg);
                $("#chance-page-box").html(msg);//替换掉id为order-data的元素的元素体  <h1><span></span> </h1>
                //3.替换分页数据
            }
        });
    }
</script>
