<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 2023/7/28
  Time: 9:47 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../common/taglib.jsp"%>
<html>
<head>
    <title>jb-aptech毕业设计项目</title>
    <script src="${basePath}/script/jquery-1.12.4.min.js"></script>
    <link href="${basePath}/css/style.css" rel="stylesheet" type="text/css">
    <script src="${basePath}/script/common.js"></script>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>

<div class="page_title">销售机会管理&nbsp; &gt; 编辑销售机会</div>
<div class="button_bar">
    <button class="common_button" onclick="help('');">帮助</button>
    <button class="common_button" onclick="back();">返回</button>
    <button class="common_button" onclick="save('${basePath}/chance/edit');">保存</button>
</div>
<form id="chanceForm">
<table class="query_form_table">
    <tr>
        <th>编号</th>
        <td><input readonly value="${salChance.getChcId()}" name="chcId" /></td>
        <th>机会来源</th>
        <td>
            <input  size="20" value="${salChance.getChcSource()}"  name="chcSource" /></td>
    </tr>
    <tr>
        <th>客户名称</th>
        <td><input value="睿智电脑" value="${salChance.getChcCustName()}"  name="chcCustName"  /><span class="red_star">*</span></td>
        <th>成功机率（%）</th>
        <td><input value="${salChance.getChcRate()}"  name="chcRate" /><span class="red_star">*</span></td>
    </tr>
    <tr>
        <th>概要</th>
        <td colspan="3"><input value="${salChance.getChcTitle()}"  name="chcTitle" size="52" /><span class="red_star">*</span></td>
    </tr>
    <tr>
        <th>联系人</th>
        <td><input name="chcLinkman" value="${salChance.getChcLinkman()}" size="20" /></td>
        <th>联系人电话</th>
        <td><input name="chcTel" value="${salChance.getChcTel()}" size="20" /></td>
    </tr>
    <tr>
        <th>机会描述</th>
        <td colspan="3"><textarea rows="6" cols="50" name="chcDesc">${salChance.getChcDesc()}</textarea><span class="red_star">*</span></td>
    </tr>
    <tr>
        <th>创建人</th>
        <td><input name="usrName"  value="${lgnUser.getUsrName()}" readonly size="20" /><span class="red_star">*</span></td>
        <th>创建时间</th>
        <td><input id="t1" name="chcCreateDate" value="<fmt:formatDate value="${salChance.getChcCreateDate()}" pattern="yyyy年MM月dd日"/>" readonly size="20" /><span class="red_star">*</span></td>
    </tr>
</table>
</form>
<br />
<table disabled class="query_form_table" id="table1">
    <tr>
        <th>指派给</th>
        <td>
            <select name="D1" disabled>
                <option>请选择...</option>
                <option>小明</option>
                <option>旺财</option>
                <option>球球</option>
                <option>孙小美</option>
                <option>周洁轮</option>
            </select> <span class="red_star">*</span></td>
        <th>指派时间</th>
        <td>
            <input  id="t2" name="T20" readonly size="20" /><span class="red_star">*</span></td>
    </tr>
</table>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">修改订单</h4>
            </div>
            <div class="modal-body">
                <p id="dialog_text">修改成功！&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    // setCurTime('t1');
    setCurTime('t2');
</script>
</body>
</html>
