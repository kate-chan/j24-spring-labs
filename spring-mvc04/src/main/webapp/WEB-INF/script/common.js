function reload(){

	//1.参数校验
	console.log($(this).serialize())
	if(($("#usrName").val()=='' && $("#title").val()=='')&&$("#linkman").val()==''){
		alert("请输入查询条件");
		return;
	}
	//2.发送请求根据用户编号或订单时间查询订单列表信息，替换body数据
	$.ajax({
		type: "GET", //请求方式 Post  get
		url: "template",//请求地址 （找得到对应的controller的方法）
		data: $("form").serialize(),//请求参数
		success: function(msg){//成功后的响应  msg返回的数据
			// alert( "Data Saved: " + msg );
			console.log(msg);
			$("#chance-page-box").html(msg);//替换掉id为order-data的元素的元素体  <h1><span></span> </h1>
			//3.替换分页数据
		}
	});


	return false;
	// window.location.reload();
}
function help(msg){
	alert('欢迎使用'+msg);
}

function to(url){
	window.location.href=url;
}
function asycTo(method,url,param){
	$.ajax({
		type: method, //请求方式 Post  get
		url: url,//请求地址 （找得到对应的controller的方法）
		data: param,//请求参数
		success: function(resp){//成功后的响应  msg返回的数据
			console.log(resp);
			if(resp.code==0){
				// alert("修改成功");
				$('#myModal').modal({
					keyboard: false,
					show:true
				})
				$('#myModal').on('hidden.bs.modal', function (e) {
					location.href="../list";  //OrderController#list方法
				})
				return ;
			}
			//如果失败，弹窗失败，无需跳转
			// alert("修改失败")
			$("#dialog_text").text('修改失败，请重试......');
			$('#myModal').modal({
				keyboard: false,
				show:true
			})
		}
	})
}
function back(){
	history.go(-1);
}
function save(url){
	var param = $("#chanceForm").serialize();
	asycTo("PUT",url,param);

}
function add(url){
	alert('新建成功！');
	to(url);
}
function del(msg){
	if (window.confirm("确认删除"+msg+"？")){
		reload();
	}
}
function setCurTime(oid){
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth();
	var day=now.getDate();
	var hours=now.getHours();
	var minutes=now.getMinutes();
	var seconds=now.getSeconds();
	var timeString = year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
	var oCtl = document.getElementById(oid);
	oCtl.value = timeString;
}