<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 2023/7/24
  Time: 11:44 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%--默认是忽略el表达式(${}),不要忽略el表达式${}--%>
<%@ page isELIgnored="false"%>
<%--jstl的日期库--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div class="col-md-12 column" id="tbl_order">
    <table class="table table-bordered table-hover">
        <caption style="text-align: center"><h3>订单管理系统</h3></caption>

        <thead>
        <tr style="border: 0px">
            <td colspan="7">
                <div style="float: left">
                    <a href="to_add" class="glyphicon glyphicon-plus" style="margin: 0px 10px"></a>
                    <a href="javascript:doCheck()" class="glyphicon glyphicon-minus" ></a>
                </div>
                <div style="float: right">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="userId">用户编号</label>
                            <input type="number" id="userId" value="${orderQueryForm.userId}" class="form-control"  name="userId" placeholder="用户编号">
                        </div>
                        <div class="form-group">
                            <label for="createtime">下单时间</label>
                            <input type="date" class="form-control" value="<fmt:formatDate value="${orderQueryForm.createtime}" pattern="yyyy-MM-dd"/>" id="createtime" name="createtime" placeholder="下单时间">
                        </div>
                        <button type="submit" class="btn btn-default">查询</button>
                    </form>
                </div>
            </td>
        </tr>
        <tr>
            <th>
                <input type="checkbox"  name="all" id="all" />
            </th>
            <th>
                编号
            </th>
            <th>
                用户名
            </th>
            <th>
                订单时间
            </th>
            <th>
                数量
            </th>
            <th>
                备注
            </th>
            <th>
                操作
            </th>
        </tr>
        </thead>
        <tbody id="order-data">
        <%--  items:对应model中key,通过key获取value
              var:临时变量，存储是逐个迭代的Order类对象
              varStatus：集合中每个元素的状态  varStatus.index获取每个元素在集合中的下表
              --%>
        <c:forEach  var="order" items="${orderPageInfo.getList()}" varStatus="status">
            <tr class="${status.index%3>1?'active':status.index%3==1?'info':'warning'}">
                <td>
                    <input type="checkbox" name="select" id="cb_${order.id}" value="${order.id}" />
                </td>
                <td>
                        ${order.id}
                </td>
                <td>
                        ${order.userInfo.username}
                </td>
                <td>
                    <fmt:formatDate value="${order.createtime}" pattern="yyyy年MM月dd日 HH:mm:ss"/>
                </td>
                <td>
                        ${order.number}
                </td>
                <td>
                        ${order.note}
                </td>
                <th>
                    <a href="info?orderId=${order.id}" class="glyphicon glyphicon-pencil" style="margin: 0px 5px"></a>
                    <a href="javascript:doDel(${order.id})" class="glyphicon glyphicon-trash" style="margin: 0px 5px"></a>
                </th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div style="text-align: right;">
        <ul class="pagination" id="page-order">
            <li class="${orderPageInfo.getPageNum()==1?'disabled':''}" >
                <a href="javascript:doPage(${orderPageInfo.getPageNum()-1})" >上一页</a>
            </li>

            <c:forEach begin="1" end="${orderPageInfo.getPages()}" step="1" var="page">

                <li class="${orderPageInfo.getPageNum()==page?'active':''}">
                    <a href="javascript:doPage(${page})" >${page}</a>
                </li>
            </c:forEach>
            <li class="${orderPageInfo.getPageNum()==orderPageInfo.getPages()?'disabled':''}">
                <a href="javascript:doPage(${orderPageInfo.getPageNum()+1})"  >下一页</a>
            </li>
        </ul>
    </div>
    <a href="export" style="margin-right: 15px">导出数据(pdf/execl)</a>
    <a href="export-hu" style="margin-right: 15px">导出数据(pdf/execl)</a>
    <a href="export-easy" style="margin-right: 15px">导出数据(pdf/execl)</a>
    <a href="import">导入数据(pdf/execl)</a>
</div>
</body>
</html>

<script type="text/javascript">
    //全选/反选
    $("#all").click(function(){
        //1.全选：权限按钮状态，选中或未选中
        //id属性值以cb_开始的元素，添加属性checked，值为：全选按钮的是否选中的值
        $("[id^='cb_']").prop("checked",$(this).prop("checked"));


    })
    //2.反选：
    var $inputs = $("input[type=checkbox]:not(#all)");
    $inputs.click(function(){
        var ch=$("input:not(#all):checked");
        $("#all").prop("checked",ch.length==$inputs.length);
    })

    function openModal(){
        $('#myModal').modal('toggle')
    }
    $("li[class=disabled] a").removeAttr("href");
    function doDel(oid){
        if(confirm("确定删除吗？")){
            location.href="delete?orderId="+oid;
        }
    }
    function doCheck(){
        //1.判断是否有选中
        if($("input:checked").size()==0){
            alert("沙雕，没选中一个怎么批量删！！！");
            return;
        }

        //2.第1步成立，则发送批量删除的请求  id=100&id=111&id=123.....
        var ids="";
        $("input[name=select]:checked").each(function (){
            ids=ids+"ids="+$(this).val()+"&";
        })
        ids = ids.substr(0,ids.lastIndexOf("&"))
        alert(ids)
        if(confirm("确定删除吗？")){
            //http://localhost:8080/项目名/order/deleteBatch?id=100&id=1111
            location.href="delete_batch?"+ids;
        }
    }
    $("form").submit(function(){
        //1.参数校验
        console.log($(this).serialize())
        if($("#userId").val()=='' && $("#createtime").val()==''){
            alert("请输入查询条件");
            return;
        }
        //2.发送请求根据用户编号或订单时间查询订单列表信息，替换body数据
        $.ajax({
            type: "GET", //请求方式 Post  get
            url: "list",//请求地址 （找得到对应的controller的方法）
            data: $(this).serialize(),//请求参数
            success: function(msg){//成功后的响应  msg返回的数据
                // alert( "Data Saved: " + msg );
                console.log(msg);
                $("#order-page-box").html(msg);//替换掉id为order-data的元素的元素体  <h1><span></span> </h1>
                //3.替换分页数据
            }
        });


        return false;
    })
    function doPage(psize){
        //1.拼接获取到的获取表单值和分页
        var param = "psize="+psize;
        if(!($("#userId").val()=='' && $("#createtime").val()=='')){
           param+="&"+ $("form").serialize();
        }
        //2.发送异步请求
        $.ajax({
            type: "GET", //请求方式 Post  get
            url: "list",//请求地址 （找得到对应的controller的方法）
            data: param,//请求参数
            success: function(msg){//成功后的响应  msg返回的数据
                // alert( "Data Saved: " + msg );
                console.log(msg);
                $("#order-page-box").html(msg);//替换掉id为order-data的元素的元素体  <h1><span></span> </h1>
                //3.替换分页数据
            }
        });
    }
    function doExport(psize){
        //1.拼接获取到的获取表单值和分页
        var param = "psize="+psize;
        if(!($("#userId").val()=='' && $("#createtime").val()=='')){
            param+="&"+ $("form").serialize();
        }
        //2.发送异步请求
        $.ajax({
            type: "GET", //请求方式 Post  get
            url: "/export",//请求地址 （找得到对应的controller的方法）
            data: param,//请求参数
            success: function(msg){//成功后的响应  msg返回的数据
                // alert( "Data Saved: " + msg );
                console.log(msg);
                $("#order-page-box").html(msg);//替换掉id为order-data的元素的元素体  <h1><span></span> </h1>
                //3.替换分页数据
            }
        });
    }
</script>
