<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 2023/7/18
  Time: 10:49 上午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%--默认是忽略el表达式(${}),不要忽略el表达式${}--%>
<%@ page isELIgnored="false"%>
<%--jstl的日期库--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<html>
<head>
    <title>订单列表</title>
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
    <style rel="stylesheet" type="text/css">
        thead tr th{
          text-align: center;
        }
        tbody tr {
            text-align: center;
        }
        tbody tr th{
            text-align: center;
        }
    </style>
</head>
<body>
<%--    <h1>订单管理系统</h1>--%>
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 column" id="order-page-box">
                <jsp:include page="data.jsp"/>
            </div>
        </div>

    </div>


</body>
</html>

