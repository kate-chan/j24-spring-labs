<%--
  Created by IntelliJ IDEA.
  User: mac
  Date: 2023/7/18
  Time: 4:46 下午
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%--默认是忽略el表达式(${}),不要忽略el表达式${}--%>
<%@ page isELIgnored="false"%>
<%--jstl的日期库--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt" %>
<html>
<head>
    <title>添加商品</title>
    <script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
    <!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script src="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
</head>
<body>


<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <h3 style="margin: 50px 5px">订单管理&nbsp;&nbsp;| &nbsp; <span class="label label-default">添加订单</span></h3>
        <form class="form-horizontal">

        <div class="form-group">
            <label for="uid" class="col-sm-2 control-label">用户编号</label>
            <div class="col-sm-10">
                <select class="form-control" id="uid" name="userId">
                    <option>----请选中用户id----</option>
                    <c:forEach items="${userInfoList}" var="u">
                    <option value="${u.id}">${u.id}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="createTime" class="col-sm-2 control-label">订单时间</label>
            <div class="col-sm-10">
                <input type="date" name="createtime"   class="form-control" id="createTime" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <label for="number" class="col-sm-2 control-label">数量</label>
            <div class="col-sm-10">
                <input type="number" name="number" max="10"  class="form-control" id="number" placeholder="订单够购物数量">
            </div>
        </div>
        <div class="form-group">
            <label for="note" class="col-sm-2 control-label">备注</label>
            <div class="col-sm-10">
                <textarea class="form-control" rows="3" id="note" name="note"></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">添加</button>
            </div>
        </div>
    </form></div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">添加订单</h4>
            </div>
            <div class="modal-body">
                <p id="dialog_text">添加成功！&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
</html>
<script type="text/javascript">
    $("form").submit( function () {
        /************表单校验*******************/
        var checkResult = true;
        $("input").each(function(){
            if($(this).val()==''&&$(this).text()==''){
                checkResult=false;
            }
        })
        if(!checkResult) {
            alert("数据填写不完整！！");
            return checkResult;
        }
        /************表单校验*******************/


        // alert($(this).serialize());
        //同步方式提交表单，请求提交表单，页面【一定会】发生跳转
        // location.href="info?"+$(this).serialize();
        //异步方式提交表单，请求提交表单，页面【不会】发生跳转
        $.ajax({
            type: "POST",//指定请求方式  post  get
            url: "save",//指定 请求的地址
            data: $(this).serialize(), //指定请求携带的数据
            success: function(resp){   //请求成功后，回调的方法，resp是控制器返回的结果
                // alert( "Data Saved: " + resp );
                //如果请求成功，则弹窗，同步跳转到order/list.jsp
                // resp.code = -1;
                if(resp.code==0){
                    // alert("修改成功");
                    $('#myModal').modal({
                        keyboard: false,
                        show:true
                    })
                    $('#myModal').on('hidden.bs.modal', function (e) {
                        location.href="list";  //OrderController#list方法
                    })
                    return ;
                }
                //如果失败，弹窗失败，无需跳转
                // alert("修改失败")
                $("#dialog_text").text('添加失败，请重试......');
                $('#myModal').modal({
                    keyboard: false,
                    show:true
                })
            }
        });
        return false;
    } );
</script>

