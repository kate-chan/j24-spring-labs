package org.example.service;

import com.github.pagehelper.PageInfo;
import org.example.entity.Order;
import org.example.web.form.OrderQueryForm;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 *
 */
public interface IOrderService {
    /**
     * 分页查询订单列表,按创建时间降序
     * @param page 页数
     * @param limit 每页条数
     * @return
     */
    PageInfo<Order> selectByPage(OrderQueryForm orderQueryForm,int page, int limit);

    /**
     * 根据订单id查询订单详情
     * @param orderId 订单id
     * @return
     */
    Order info(Integer orderId);

    /**
     * 根据id更新订单信息
     * @param order 待更新订单对象
     * @return
     */
    int updateById(Order order);

    /**
     * 根据id删除
     * @param orderId 订单id
     * @return
     */
    int deleteById(Integer orderId);

    /**
     * 根据id批量删除
     * @param ids ids数组
     * @return
     */
    int deleteBatch(Integer[] ids);

    /**
     * 添加订单
     * @param order 待添加订单对象
     * @return
     */
    int save(Order order);

    /**
     * 【废弃】已经与list合并成一个方法，
     * 根据订单用户id或订单时间分页查询
     * @param orderQueryForm 查询条件
     * @param page  页数
     * @param limit 每页条数
     * @return
     */
    PageInfo<Order> search(OrderQueryForm orderQueryForm, int page, int limit);

    /**
     * 报表导出
     * @param page
     */
    void export(int page, HttpServletResponse response);

    /**
     * 报表导出--hutool
     * @param page
     */
    void exportHutool(int page, HttpServletResponse response);
    /**
     * 报表导出--hutool
     * @param page
     */
    public void exportEasyExecl(int page, HttpServletResponse response);

    /**
     * 导入excel
     * @param multipartFile
     */
    String importEasyExecl(MultipartFile multipartFile);

}
