package org.example.service;

import com.github.pagehelper.PageInfo;
import org.example.entity.SalChance;
import org.example.web.form.ChanceForm;

public interface ISaleChanceService {
    /**
     * 分页查询订单列表,按创建时间降序
     * @param page 页数
     * @param limit 每页条数
     * @return
     */
    PageInfo<SalChance> selectByPage(ChanceForm chanceForm, Integer page, int limit);

    /**
     * 根据机会id查询机会详情
     * @param chcId
     * @return
     */
    SalChance info(Integer chcId);

    /**
     * 根据机会id跟新销售机会信息
     * @param salChance
     * @return
     */
    boolean update(SalChance salChance);
}
