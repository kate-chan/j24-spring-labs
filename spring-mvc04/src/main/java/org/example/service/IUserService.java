package org.example.service;

import org.example.entity.SysUser;
import org.example.web.form.LoginForm;

import java.util.List;

public interface IUserService {
    List<SysUser> selectAllValid();

    /**
     *
     * 用户名密码登录
     * @param loginForm 用户名密码表单
     * @return
     */
    SysUser login(LoginForm loginForm);

}
