package org.example.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.example.entity.SalChance;
import org.example.mapper.SaleChanceMapper;
import org.example.service.ISaleChanceService;
import org.example.web.form.ChanceForm;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SaleChanceServiceImpl implements ISaleChanceService {

    @Resource
    private SaleChanceMapper chanceMapper;
    @Override
    public PageInfo<SalChance> selectByPage(ChanceForm chanceForm, Integer page, int limit) {
        PageHelper.startPage(page,limit);
        List<SalChance> salChanceList = chanceMapper.selectByUserIdOrCreatetime(chanceForm);
        PageInfo<SalChance> salChancePageInfo = new PageInfo<>(salChanceList);
        return salChancePageInfo;
    }

    @Override
    public SalChance info(Integer chcId) {
        return chanceMapper.selectByPrimaryKey(chcId);
    }

    @Override
    public boolean update(SalChance salChance) {
        return chanceMapper.updateByPrimaryKeySelective(salChance)>=0;
    }
}
