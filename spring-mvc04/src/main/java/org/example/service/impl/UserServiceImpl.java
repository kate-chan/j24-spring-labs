package org.example.service.impl;

import cn.hutool.crypto.digest.DigestAlgorithm;
import cn.hutool.crypto.digest.Digester;
import org.example.entity.SysUser;
import org.example.mapper.SysUserMapper;
import org.example.service.IUserService;
import org.example.web.form.LoginForm;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public List<SysUser> selectAllValid() {
        return sysUserMapper.selectByExample(null);
    }

    @Override
    public SysUser login(LoginForm loginForm) {
        Digester md5 = new Digester(DigestAlgorithm.MD5);
        // 5393554e94bf0eb6436f240a4fd71282
        String password = md5.digestHex(loginForm.getPassword());//输入：pwd
        loginForm.setPassword(password);//密文：9003d1df22eb4d3820015070385194c8
        return sysUserMapper.selectByUsernameAndPwd(loginForm);
    }


}
