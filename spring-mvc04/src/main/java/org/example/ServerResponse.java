package org.example;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.Serializable;
@Data
@JsonSerialize(include =  JsonSerialize.Inclusion.NON_NULL)
public class ServerResponse<T> implements Serializable {
    private Integer code;//响应码  0表式请求成功  -1请求失败
    private String msg; //响应的补充信息 success  failed
    private T data;

    private ServerResponse(int code,String msg,T data){
        this.code =code;
        this.msg = msg;
        this.data=data;
    }
    public static <T> ServerResponse success(T data){
        return new ServerResponse(0,"success",data);
    }

    public static ServerResponse successWithMsg(){
        return new ServerResponse(0,"success",null);
    }

    public static ServerResponse invalidParam(){
        return new ServerResponse(-1,"invalid param error..... ",null);
    }

    public static ServerResponse failedWithMsg(String message) {
        return new ServerResponse(-1,message,null);
    }
}
