package org.example.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 关键点1：拦截器如何定义？
 *      注意：要导入servlet的依赖
 * 关键点2：拦截器的3个重写的方法分别在什么时候执行？
 * 关键点3：preHandle该方法值问题：
 *      1）返回false,则请求被拦截，不会执行后续的请求直接返回。
 *      2）返回true,则请求继续往下执行
 */
@Slf4j
public class MyInterceptor02 implements HandlerInterceptor {
    /**
     * 进入控制器(XXXController)之前，被调用
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
        log.info("进入MyInterceptor02。。。preHandle......参数handler：{}",handler);
        //如果你【已经登录】了，则放行，否者不拦截，并跳转到登录页

        return true;//不放行，只会经过preHandle这一个方法，其他方法和控制器都不会进
    }

    /**
     * 执行完对应控制器，控制器返回之前
     * @param request
     * @param response
     * @param handler
     * @param modelAndView
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        log.info("进入MyInterceptor02。。。postHandle......参数handler：{},参数modelAndView：{}",handler,modelAndView);
    }

    /**
     * 控制器执行完，并且返回后
     * @param request
     * @param response
     * @param handler
     * @param ex
     * @throws Exception
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        log.info("进入MyInterceptor02。。。。afterCompletion......参数handler：{}",handler);
    }
}
