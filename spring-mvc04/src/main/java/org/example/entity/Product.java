package org.example.entity;

import java.io.Serializable;

public class Product implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer prodId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_name
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String prodName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_type
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String prodType;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_batch
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String prodBatch;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_unit
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String prodUnit;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_price
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer prodPrice;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column Product.prod_memo
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String prodMemo;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table Product
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_id
     *
     * @return the value of Product.prod_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getProdId() {
        return prodId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_id
     *
     * @param prodId the value for Product.prod_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_name
     *
     * @return the value of Product.prod_name
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getProdName() {
        return prodName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_name
     *
     * @param prodName the value for Product.prod_name
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdName(String prodName) {
        this.prodName = prodName == null ? null : prodName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_type
     *
     * @return the value of Product.prod_type
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getProdType() {
        return prodType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_type
     *
     * @param prodType the value for Product.prod_type
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdType(String prodType) {
        this.prodType = prodType == null ? null : prodType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_batch
     *
     * @return the value of Product.prod_batch
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getProdBatch() {
        return prodBatch;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_batch
     *
     * @param prodBatch the value for Product.prod_batch
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdBatch(String prodBatch) {
        this.prodBatch = prodBatch == null ? null : prodBatch.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_unit
     *
     * @return the value of Product.prod_unit
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getProdUnit() {
        return prodUnit;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_unit
     *
     * @param prodUnit the value for Product.prod_unit
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdUnit(String prodUnit) {
        this.prodUnit = prodUnit == null ? null : prodUnit.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_price
     *
     * @return the value of Product.prod_price
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getProdPrice() {
        return prodPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_price
     *
     * @param prodPrice the value for Product.prod_price
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdPrice(Integer prodPrice) {
        this.prodPrice = prodPrice;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column Product.prod_memo
     *
     * @return the value of Product.prod_memo
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getProdMemo() {
        return prodMemo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column Product.prod_memo
     *
     * @param prodMemo the value for Product.prod_memo
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setProdMemo(String prodMemo) {
        this.prodMemo = prodMemo == null ? null : prodMemo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Product
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", prodId=").append(prodId);
        sb.append(", prodName=").append(prodName);
        sb.append(", prodType=").append(prodType);
        sb.append(", prodBatch=").append(prodBatch);
        sb.append(", prodUnit=").append(prodUnit);
        sb.append(", prodPrice=").append(prodPrice);
        sb.append(", prodMemo=").append(prodMemo);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Product
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Product other = (Product) that;
        return (this.getProdId() == null ? other.getProdId() == null : this.getProdId().equals(other.getProdId()))
            && (this.getProdName() == null ? other.getProdName() == null : this.getProdName().equals(other.getProdName()))
            && (this.getProdType() == null ? other.getProdType() == null : this.getProdType().equals(other.getProdType()))
            && (this.getProdBatch() == null ? other.getProdBatch() == null : this.getProdBatch().equals(other.getProdBatch()))
            && (this.getProdUnit() == null ? other.getProdUnit() == null : this.getProdUnit().equals(other.getProdUnit()))
            && (this.getProdPrice() == null ? other.getProdPrice() == null : this.getProdPrice().equals(other.getProdPrice()))
            && (this.getProdMemo() == null ? other.getProdMemo() == null : this.getProdMemo().equals(other.getProdMemo()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table Product
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getProdId() == null) ? 0 : getProdId().hashCode());
        result = prime * result + ((getProdName() == null) ? 0 : getProdName().hashCode());
        result = prime * result + ((getProdType() == null) ? 0 : getProdType().hashCode());
        result = prime * result + ((getProdBatch() == null) ? 0 : getProdBatch().hashCode());
        result = prime * result + ((getProdUnit() == null) ? 0 : getProdUnit().hashCode());
        result = prime * result + ((getProdPrice() == null) ? 0 : getProdPrice().hashCode());
        result = prime * result + ((getProdMemo() == null) ? 0 : getProdMemo().hashCode());
        return result;
    }
}