package org.example.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class SalChance implements Serializable {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer chcId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_source
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcSource;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_cust_name
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcCustName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_title
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcTitle;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_rate
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer chcRate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_linkman
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcLinkman;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_tel
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcTel;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_desc
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcDesc;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_create_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer chcCreateId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_create_by
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcCreateBy;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_create_date
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @DateTimeFormat(pattern = "yyyy年MM月dd日")
    private Date chcCreateDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_due_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer chcDueId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_due_to
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private String chcDueTo;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_due_date
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Date chcDueDate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sal_chance.chc_status
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private Integer chcStatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table sal_chance
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    private static final long serialVersionUID = 1L;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_id
     *
     * @return the value of sal_chance.chc_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getChcId() {
        return chcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_id
     *
     * @param chcId the value for sal_chance.chc_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcId(Integer chcId) {
        this.chcId = chcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_source
     *
     * @return the value of sal_chance.chc_source
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcSource() {
        return chcSource;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_source
     *
     * @param chcSource the value for sal_chance.chc_source
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcSource(String chcSource) {
        this.chcSource = chcSource == null ? null : chcSource.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_cust_name
     *
     * @return the value of sal_chance.chc_cust_name
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcCustName() {
        return chcCustName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_cust_name
     *
     * @param chcCustName the value for sal_chance.chc_cust_name
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcCustName(String chcCustName) {
        this.chcCustName = chcCustName == null ? null : chcCustName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_title
     *
     * @return the value of sal_chance.chc_title
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcTitle() {
        return chcTitle;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_title
     *
     * @param chcTitle the value for sal_chance.chc_title
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcTitle(String chcTitle) {
        this.chcTitle = chcTitle == null ? null : chcTitle.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_rate
     *
     * @return the value of sal_chance.chc_rate
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getChcRate() {
        return chcRate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_rate
     *
     * @param chcRate the value for sal_chance.chc_rate
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcRate(Integer chcRate) {
        this.chcRate = chcRate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_linkman
     *
     * @return the value of sal_chance.chc_linkman
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcLinkman() {
        return chcLinkman;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_linkman
     *
     * @param chcLinkman the value for sal_chance.chc_linkman
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcLinkman(String chcLinkman) {
        this.chcLinkman = chcLinkman == null ? null : chcLinkman.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_tel
     *
     * @return the value of sal_chance.chc_tel
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcTel() {
        return chcTel;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_tel
     *
     * @param chcTel the value for sal_chance.chc_tel
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcTel(String chcTel) {
        this.chcTel = chcTel == null ? null : chcTel.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_desc
     *
     * @return the value of sal_chance.chc_desc
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcDesc() {
        return chcDesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_desc
     *
     * @param chcDesc the value for sal_chance.chc_desc
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcDesc(String chcDesc) {
        this.chcDesc = chcDesc == null ? null : chcDesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_create_id
     *
     * @return the value of sal_chance.chc_create_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getChcCreateId() {
        return chcCreateId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_create_id
     *
     * @param chcCreateId the value for sal_chance.chc_create_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcCreateId(Integer chcCreateId) {
        this.chcCreateId = chcCreateId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_create_by
     *
     * @return the value of sal_chance.chc_create_by
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcCreateBy() {
        return chcCreateBy;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_create_by
     *
     * @param chcCreateBy the value for sal_chance.chc_create_by
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcCreateBy(String chcCreateBy) {
        this.chcCreateBy = chcCreateBy == null ? null : chcCreateBy.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_create_date
     *
     * @return the value of sal_chance.chc_create_date
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Date getChcCreateDate() {
        return chcCreateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_create_date
     *
     * @param chcCreateDate the value for sal_chance.chc_create_date
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcCreateDate(Date chcCreateDate) {
        this.chcCreateDate = chcCreateDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_due_id
     *
     * @return the value of sal_chance.chc_due_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getChcDueId() {
        return chcDueId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_due_id
     *
     * @param chcDueId the value for sal_chance.chc_due_id
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcDueId(Integer chcDueId) {
        this.chcDueId = chcDueId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_due_to
     *
     * @return the value of sal_chance.chc_due_to
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public String getChcDueTo() {
        return chcDueTo;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_due_to
     *
     * @param chcDueTo the value for sal_chance.chc_due_to
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcDueTo(String chcDueTo) {
        this.chcDueTo = chcDueTo == null ? null : chcDueTo.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_due_date
     *
     * @return the value of sal_chance.chc_due_date
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Date getChcDueDate() {
        return chcDueDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_due_date
     *
     * @param chcDueDate the value for sal_chance.chc_due_date
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcDueDate(Date chcDueDate) {
        this.chcDueDate = chcDueDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sal_chance.chc_status
     *
     * @return the value of sal_chance.chc_status
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public Integer getChcStatus() {
        return chcStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sal_chance.chc_status
     *
     * @param chcStatus the value for sal_chance.chc_status
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    public void setChcStatus(Integer chcStatus) {
        this.chcStatus = chcStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sal_chance
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", chcId=").append(chcId);
        sb.append(", chcSource=").append(chcSource);
        sb.append(", chcCustName=").append(chcCustName);
        sb.append(", chcTitle=").append(chcTitle);
        sb.append(", chcRate=").append(chcRate);
        sb.append(", chcLinkman=").append(chcLinkman);
        sb.append(", chcTel=").append(chcTel);
        sb.append(", chcDesc=").append(chcDesc);
        sb.append(", chcCreateId=").append(chcCreateId);
        sb.append(", chcCreateBy=").append(chcCreateBy);
        sb.append(", chcCreateDate=").append(chcCreateDate);
        sb.append(", chcDueId=").append(chcDueId);
        sb.append(", chcDueTo=").append(chcDueTo);
        sb.append(", chcDueDate=").append(chcDueDate);
        sb.append(", chcStatus=").append(chcStatus);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sal_chance
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SalChance other = (SalChance) that;
        return (this.getChcId() == null ? other.getChcId() == null : this.getChcId().equals(other.getChcId()))
            && (this.getChcSource() == null ? other.getChcSource() == null : this.getChcSource().equals(other.getChcSource()))
            && (this.getChcCustName() == null ? other.getChcCustName() == null : this.getChcCustName().equals(other.getChcCustName()))
            && (this.getChcTitle() == null ? other.getChcTitle() == null : this.getChcTitle().equals(other.getChcTitle()))
            && (this.getChcRate() == null ? other.getChcRate() == null : this.getChcRate().equals(other.getChcRate()))
            && (this.getChcLinkman() == null ? other.getChcLinkman() == null : this.getChcLinkman().equals(other.getChcLinkman()))
            && (this.getChcTel() == null ? other.getChcTel() == null : this.getChcTel().equals(other.getChcTel()))
            && (this.getChcDesc() == null ? other.getChcDesc() == null : this.getChcDesc().equals(other.getChcDesc()))
            && (this.getChcCreateId() == null ? other.getChcCreateId() == null : this.getChcCreateId().equals(other.getChcCreateId()))
            && (this.getChcCreateBy() == null ? other.getChcCreateBy() == null : this.getChcCreateBy().equals(other.getChcCreateBy()))
            && (this.getChcCreateDate() == null ? other.getChcCreateDate() == null : this.getChcCreateDate().equals(other.getChcCreateDate()))
            && (this.getChcDueId() == null ? other.getChcDueId() == null : this.getChcDueId().equals(other.getChcDueId()))
            && (this.getChcDueTo() == null ? other.getChcDueTo() == null : this.getChcDueTo().equals(other.getChcDueTo()))
            && (this.getChcDueDate() == null ? other.getChcDueDate() == null : this.getChcDueDate().equals(other.getChcDueDate()))
            && (this.getChcStatus() == null ? other.getChcStatus() == null : this.getChcStatus().equals(other.getChcStatus()));
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sal_chance
     *
     * @mbg.generated Mon Dec 28 15:48:49 CST 2020
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getChcId() == null) ? 0 : getChcId().hashCode());
        result = prime * result + ((getChcSource() == null) ? 0 : getChcSource().hashCode());
        result = prime * result + ((getChcCustName() == null) ? 0 : getChcCustName().hashCode());
        result = prime * result + ((getChcTitle() == null) ? 0 : getChcTitle().hashCode());
        result = prime * result + ((getChcRate() == null) ? 0 : getChcRate().hashCode());
        result = prime * result + ((getChcLinkman() == null) ? 0 : getChcLinkman().hashCode());
        result = prime * result + ((getChcTel() == null) ? 0 : getChcTel().hashCode());
        result = prime * result + ((getChcDesc() == null) ? 0 : getChcDesc().hashCode());
        result = prime * result + ((getChcCreateId() == null) ? 0 : getChcCreateId().hashCode());
        result = prime * result + ((getChcCreateBy() == null) ? 0 : getChcCreateBy().hashCode());
        result = prime * result + ((getChcCreateDate() == null) ? 0 : getChcCreateDate().hashCode());
        result = prime * result + ((getChcDueId() == null) ? 0 : getChcDueId().hashCode());
        result = prime * result + ((getChcDueTo() == null) ? 0 : getChcDueTo().hashCode());
        result = prime * result + ((getChcDueDate() == null) ? 0 : getChcDueDate().hashCode());
        result = prime * result + ((getChcStatus() == null) ? 0 : getChcStatus().hashCode());
        return result;
    }
}