package org.example.web;

import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.example.ServerResponse;
import org.example.entity.SalChance;
import org.example.service.ISaleChanceService;
import org.example.web.form.ChanceForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Controller
@RequestMapping("/chance")
@Slf4j
public class ChanceController {

    @Resource
    private ISaleChanceService saleChanceService;
    /**
     * 分页查询机会信息列表
     * @param page  页数
     * @param limit 每页条数
     * @return
     */
    @GetMapping("/template")
    public String list(ChanceForm chanceForm, Model model ,
                       @RequestParam(value = "psize",defaultValue = "1") int page,
                       @RequestParam(defaultValue = "5")int limit){
        //1.参数校验-----默认值
        //2.调用service
        PageInfo<SalChance> salChancePageInfo = saleChanceService.selectByPage(chanceForm, page, limit);
        log.info("orderPageInfo=========>{}",salChancePageInfo);
        //3.返回model(数据)和view(网页) request.setAttribute(key,value)
        model.addAttribute("salChancePageInfo",salChancePageInfo);//key-value
        return "sales/listTemplate";
    }
    @GetMapping("/info/{chcId}")
    public String info(@PathVariable("chcId") Integer chcId, Model model){
        log.info("进入info....参数chcId======{}",chcId);
        //1.参数校验-----默认值
        if(chcId==null){
            throw new RuntimeException("非法参数");
        }
        //2.调用service
        SalChance salChance =  saleChanceService.info(chcId);
        log.info("查询的salChance信息为========>{}"+salChance);
        if(salChance==null){
            throw new RuntimeException("销售机会不存在....");
        }
        //3.返回model(数据)和view(网页) request.setAttribute(key,value)
        model.addAttribute("salChance",salChance);
        return "sales/edit";
    }

    @PutMapping("/edit")
    @ResponseBody
    public ServerResponse edit(SalChance salChance){
        log.info("进入edit....参数salChance======{}",salChance);
        //1.参数校验-----默认值
        if(!checkEditSaleChance(salChance)){
            return ServerResponse.failedWithMsg("参数错误");
        }
        //2.调用service
        boolean result = saleChanceService.update(salChance);
        //3.返回model(数据)和view(网页) request.setAttribute(key,value)
        if(!result){
            return ServerResponse.failedWithMsg("更新销售机会失败");
        }
        return ServerResponse.successWithMsg();
    }

    private boolean checkEditSaleChance(SalChance salChance) {
        //根据需求文档进行校验
        return true;
    }


    private boolean checkOrderQueryForm(ChanceForm chanceForm) {
        return !((chanceForm.getLinkman()==null||chanceForm.getLinkman().trim().length()==0)
                &&(chanceForm.getTitle()==null||chanceForm.getTitle().trim().length()==0)
                &&(chanceForm.getCustName()==null||chanceForm.getCustName().trim().length()==0));
    }
}
