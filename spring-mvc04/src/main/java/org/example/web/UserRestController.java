package org.example.web;

import lombok.extern.slf4j.Slf4j;
import org.example.entity.UserInfo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
@Slf4j
public class UserRestController {
    //添加  请求路径：/add  请求方式：post
//    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @PostMapping("/") //请求方式：post  操作：添加
    public String add(UserInfo userInfo){
        log.info("进行添加操作......，参数userInfo:{}",userInfo);
        return "index";
    }

    @PutMapping("/") //请求方式：put  操作：更新操作
    public String update(UserInfo userInfo){
        log.info("进行更新操作......参数userInfo:{}",userInfo);
        return "index";
    }

    @DeleteMapping("/{id}") //请求方式：delete  操作：删除服务器资源
    public String delete(@PathVariable("id") Integer id){
        log.info("进行删除操作......参数id:{}",id);
        return "index";
    }

    @GetMapping("/") //请求方式:get  操作：获取服务器资源
    public String list(@RequestParam(defaultValue = "1") int page,@RequestParam(defaultValue = "10")int limit){
        log.info("进行分页查询操作......参数page:{},参数limit:{}",page,limit);
        return "index";
    }

    @GetMapping("/{id}") //请求方式：get  操作：获取服务器资源
    public String info(@PathVariable("id") Integer id){
        log.info("进行分页查询操作......参数id:{}",id);
        return "index";
    }
}
