package org.example.web;


import lombok.extern.slf4j.Slf4j;
import org.example.ServerResponse;
import org.example.entity.UserInfo;
import org.springframework.web.bind.annotation.*;

//@Controller
@RestController
@RequestMapping("/user2")
@Slf4j
public class UserJsonController {
    /**
     * 请求传参，通过json  @RequestBody
     * 响应：json        @ResponseBody
     * @param userInfo
     * @return
     */
    @PutMapping("/")
//    @ResponseBody
    public ServerResponse update(@RequestBody UserInfo userInfo){
        log.info("进入UserJsonController的update，参数为：======>{}",userInfo);
        return ServerResponse.successWithMsg();
    }
}

