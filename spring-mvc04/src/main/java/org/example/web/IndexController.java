package org.example.web;
import lombok.extern.slf4j.Slf4j;
import org.example.entity.SysUser;
import org.example.service.IUserService;
import org.example.web.form.LoginForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@Slf4j
@RequestMapping("/index")
public class IndexController {
    @Resource
    private IUserService userService;

    //http://localhost:8080/项目明/index-----》IndexController#index方法
    @RequestMapping("/index")
    public String index(){
        log.info("IndexController处理器指定了index方法...");
        //默认返回的视图的名称 ，最后通过前缀+视图名称+后缀，得到网页
        return "index";///WEB-INF/index.jsp
    }
//    @RequestMapping("/to_login")
//    public String toLogin(){return "login";}
    @RequestMapping("/login")
    public String index(LoginForm loginForm, HttpServletRequest request){
        log.info("参数loginForm为===============>{}",loginForm);
        //1.参数校验
        if(checkLoginParameter(loginForm)){
            return "redirect:/login";//redirect：重定向  forward:转发
        }
        //2.调用service
        SysUser sysUserLgn = userService.login(loginForm);
        if(sysUserLgn==null){
            return "redirect:/login";
        }
        //保存登录的状态---？
        // 1.用什么保存？cookie（前端）  request(一次请求范围有效)  session(一次会话范围有效，无数据交互30分钟) application(服务器容器销毁前有效)
        //域对象使用原则：能用小的就不要用大的。session
        HttpSession session = request.getSession();
        // 2.保存什么东西？----存储用户信息，脱敏(密码)
        sysUserLgn.setUsrPassword(null);
        session.setAttribute("lgnUser",sysUserLgn);//key---value
        //3.返回model和view---?
        return "redirect:/index";
    }

    private boolean checkLoginParameter(LoginForm loginForm) {
        //TODO 省去1000字
        return false;
    }

}
