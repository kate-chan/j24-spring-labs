package org.example.web.form;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class OrderQueryForm implements Serializable {
    private Integer userId;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createtime;

}
