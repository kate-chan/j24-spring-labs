package org.example.web.form;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChanceForm implements Serializable {
    private String linkman;
    private String title;
    private String custName;
}
