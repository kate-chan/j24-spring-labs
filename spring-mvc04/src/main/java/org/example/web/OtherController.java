package org.example.web;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/other")
@Slf4j
public class OtherController {
    /**
     * 一、mvc处理器返回值类型为：ModelAndView
     * @param model
     * @return
     */
    @RequestMapping("/test01")
    public ModelAndView test01(Model model){
        log.info("进入test01，返回类型为：ModelAndView");
        //1.校验
        //2.调用Service
        //3.返回数据(model)和视图(view)
//        model.addAttribute("abc","abc");
//        return "index"; 前提是方法返回值是：String
        ModelAndView mv = new ModelAndView();
        mv.addObject("abc","abc");
        mv.setViewName("index");
        return mv;
    }

    /**
     * 二、mvc处理器返回值类型为：void
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping("/test02")
    public void test02(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        log.info("进入test02，返回类型为：Void");
        //跳转到index.jsp
        //转发
        request.getRequestDispatcher("/index.jsp").forward(request,response);;
        //重定向
//        response.sendRedirect(request.getContextPath()+"/index.jsp");
    }
    /**
     *
     *三、路径参数
     * 以前：
     *      http://localhost:8080/项目名/other/info?id=23
     * 路劲参数
     *      http://localhost:8080/项目名/other/info/23
     */
    @RequestMapping("/info/{id}")
    public String test03(@PathVariable(value = "id") Integer id){
        log.info("进入test03，参数id为：{}",id);
        return "index";
    }
    /**
     *
     *四、servlet相关对象
     * 1.request
     * 2.reponse
     * 3.session
     * 4.applicaion
     * 5.获取cookie值
     * 6.获取请求头的值
     */
    @RequestMapping("/test04")
    public String test04(HttpServletRequest request,
                         HttpServletResponse response,
                         HttpSession session,
                         @RequestHeader("accept") String acceptValue,
                         @CookieValue("JSESSIONID") String sessionId){
       //1.request,response,session
        log.info("request==>{},response==>{},session==>{}",request,response,session);
        //2.cookie
        log.info("cookie中sessionId为：==>{}",sessionId);
        //3.请求头  请求头中accept的值
        log.info("请求头中accept值为：==>{}",acceptValue);

        return "index";
    }

}
