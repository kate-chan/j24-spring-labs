package org.example.exception;

public class OrderHandlerException extends RuntimeException{
    public OrderHandlerException(String message) {
        super(message);
    }
}
