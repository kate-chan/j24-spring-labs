package com.kate.ioc;
import com.github.pagehelper.PageHelper;
import com.kate.ioc.entity.Order;
import com.kate.ioc.mapper.OrderMapper;
import com.kate.ioc.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:beans.xml")
public class TestMapper {
    //junit测试
    @Test
    public void testUserMapper(){
        //1.工厂
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        UserMapper userMapper = (UserMapper) context.getBean("userMapper");
        System.out.println(userMapper.selectAll());

    }
    //spring-test+junit

    @Resource
    private UserMapper userMapper;

    @Test
    public void testUserMapper2(){
        System.out.println(userMapper.selectAll());
    }

    @Resource
    private OrderMapper orderMapper;
    @Test
    public void testOrderMapper(){
        List<Order> orders = orderMapper.selectById(100);
        System.out.println(orders);
    }

    @Test
    public void testOrderMapperPage(){
        PageHelper.startPage(2,2);
        List<Order> orders = orderMapper.selectById(100);
        System.out.println(orders);
    }


}
