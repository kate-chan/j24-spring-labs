package com.kate.ioc;


import com.github.pagehelper.PageHelper;
import com.kate.ioc.config.AppConfig;
import com.kate.ioc.entity.Order;
import com.kate.ioc.mapper.OrderMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class TestMapper2 {
    @Resource
    private OrderMapper orderMapper;
    @Test
    public void testOrderMapper(){
        List<Order> orders = orderMapper.selectById(100);
        System.out.println(orders);
    }

    @Test
    public void testOrderMapperPage(){
        PageHelper.startPage(2,2);
        List<Order> orders = orderMapper.selectById(100);
        System.out.println(orders);
    }
}
