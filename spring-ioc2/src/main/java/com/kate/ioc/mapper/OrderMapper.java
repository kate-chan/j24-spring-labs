package com.kate.ioc.mapper;

import com.kate.ioc.entity.Order;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface OrderMapper {

    @Select("select * from `order` where id = #{id}")
    List<Order> selectById(Integer id);
}
