package com.kate.ioc.entity;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private Integer id;
    private String username;
    private String address;
    private Date birthday;
    private String sex;
}
