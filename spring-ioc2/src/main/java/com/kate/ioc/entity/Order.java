package com.kate.ioc.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Order {
    private Integer id;
    private Integer UserId;
    private Integer number;
    private Date createtime;
    private String note;
}
