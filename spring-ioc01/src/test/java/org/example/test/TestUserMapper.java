package org.example.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.example.entity.User;
import org.example.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;


@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:beans-datasource.xml")// ApplicationContext context = new  ClassPathXmlApplicationContext("beans-datasource.xml");
public class TestUserMapper {

    @Test
    public void testFindById() throws IOException {
        //TODO 1.读取mybatis核心配置文件
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);

        //TODO 2.构建SqlSessionFactory
        final SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        //TODO 3.构建sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession(true);
        //TODO  4.UserMapper
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        //TODO  5.调用userMapper方法
        User user = userMapper.findById(1);
        System.out.println(user);
        //TODO  6.回收资源
        sqlSession.close();;
    }

    @Resource
    private UserMapper userMapper;//UserMapper userMapper = (UserMapper) context.getBean("userMapper");
    @Test
    public void test01(){
//        ApplicationContext context = new  ClassPathXmlApplicationContext("beans-datasource.xml");
//        UserMapper userMapper = (UserMapper) context.getBean("userMapper");
        System.out.println(userMapper.findById(1));
    }





}
