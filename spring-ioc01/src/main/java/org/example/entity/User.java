package org.example.entity;

import lombok.Data;
import org.mybatis.spring.SqlSessionTemplate;

/**
 * @author 新梦想.陈超
 * @version 2021.2
 * @Description: {数据库表的实体类，lombok插件}
 * @date 2023/6/30 下午3:32
 */
@Data   //自动生成get/set toString equals hashcode
public class User {
    SqlSessionTemplate sqlSessionTemplat;
    private Integer id;
    private String username;
    private String birthday;
    private String sex;
    private String address;
    private int user_status;
    private String gallery_url;
}
