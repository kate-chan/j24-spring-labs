package org.example.mapper;

import org.example.entity.User;

public interface UserMapper {
    User findById(Integer id);
}
