package com.kate.base.factory;

import com.kate.base.pojo.BlackPig;
import com.kate.base.pojo.RedPig;

public class InstanceFactory {
    public static RedPig getRedPig() {
        return new RedPig(2,"赵二狗");
    }
    public BlackPig getBlackPig() {
        return new BlackPig(3,"刘大锤");
    }


}
