package com.kate.base.factory;

public class ObjectFactory	 {
 
    //使用方法重载来实现，每个方法负责生成一种Pig （最low）
//    public static RedPig getRegPig() {
//        return  new RedPig(12, "旺财");
//    }
//    public static BlackPig getBlackPig() {
//        return  new BlackPig(11, "来福");
//    }
//
//    public static BluePig getBluePig() {
//        return  new BluePig(11, "来福");
//    }


    public Object instance(Class clazz) throws IllegalAccessException, InstantiationException {
        Object obj = clazz.newInstance();
        return obj;
    }


}