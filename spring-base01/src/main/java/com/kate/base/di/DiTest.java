package com.kate.base.di;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiTest {
    public static void main(String[] args) {
        String[] data = {"asdfasdfasdfsadf油温1：49度 油温2：50度asdfasdfsadf", "asdfasdfasdfsadf油温1：52度 油温2：掉线,油位：599.9"};

        String temperaturePattern = "油温\\d+：(\\d+度)";
        String offlinePattern = "油温\\d+：掉线";

        Pattern temperatureRegex = Pattern.compile(temperaturePattern);
        Pattern offlineRegex = Pattern.compile(offlinePattern);

        for (String str : data) {
            Matcher temperatureMatcher = temperatureRegex.matcher(str);
            Matcher offlineMatcher = offlineRegex.matcher(str);

            if (temperatureMatcher.find()) {
                String temperature = temperatureMatcher.group(1);
                System.out.println("油温数据：" + temperature);
            }

            if (offlineMatcher.find()) {
                System.out.println("掉线状态");
            }
        }
    }
}
