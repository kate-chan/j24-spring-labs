package com.kate.base.di;

import com.kate.base.pojo.di.Dog;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-scope.xml");
        //测试bean的作用域
        Dog ddg01 = (Dog) context.getBean("dg01");
        Dog ddg02 = (Dog) context.getBean("dg01");
    }
}
