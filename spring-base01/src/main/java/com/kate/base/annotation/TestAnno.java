package com.kate.base.annotation;
import com.kate.base.pojo.annotation.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class TestAnno {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-annotation.xml");
        User u1 = (User) context.getBean("user");
        System.out.println(u1);
        
        User u2 = (User) context.getBean("user");
        System.out.println(u2);

        ((ClassPathXmlApplicationContext)context).close();
    }
}
