package com.kate.base.singleton;

/**
 * 单例模式实现方式一：懒汉式
 *
 * 问题：线程安全问题
 */
public class EagerSingleton {

    //2.定义静态常量引用
    private static final EagerSingleton instance = new EagerSingleton();
    //1.构造方法私有
    private EagerSingleton(){

    }
    //3.提供公有的返回单列的方法
    public static EagerSingleton getInstance(){
        return instance;
    }
}
