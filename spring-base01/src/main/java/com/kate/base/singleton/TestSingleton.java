package com.kate.base.singleton;

public class TestSingleton {
    public static void main(String[] args) {
        //hashcode码：对象在内存中地址的整数形式，怎么获取hashcode码？
        //通过对象的hashcode()方法就可以返回hashcode码
//        EagerSingleton eagerSingleton =new EagerSingleton();
        //1.饿汉式的类的对象的hashcode吗
//        EagerSingleton instance01 = EagerSingleton.getInstance();
//
//        EagerSingleton instance02 = EagerSingleton.getInstance();
//        EagerSingleton instance03 = EagerSingleton.getInstance();
//        EagerSingleton instance04 = EagerSingleton.getInstance();
//
//        System.out.println(instance01.hashCode());
//        System.out.println(instance02.hashCode());
//        System.out.println(instance03.hashCode());
//        System.out.println(instance04.hashCode());
//
//
//
//        //非单列模式类对象hashcode码
//        RedPig redPig1 = new RedPig();
//        RedPig redPig2 = new RedPig();
//        RedPig redPig3 = new RedPig();
//        RedPig redPig4 = new RedPig();
//        System.out.println(redPig1.hashCode());
//        System.out.println(redPig2.hashCode());
//        System.out.println(redPig3.hashCode());
//        System.out.println(redPig4.hashCode());
//
//        //3.懒汉式的类对象hashcode码
//        LazySingleton lazySingleton01 = LazySingleton.getInstance();
//        LazySingleton lazySingleton02 = LazySingleton.getInstance();
//        LazySingleton lazySingleton03 = LazySingleton.getInstance();
//        LazySingleton lazySingleton04 = LazySingleton.getInstance();
//
//        System.out.println(lazySingleton01.hashCode());
//        System.out.println(lazySingleton02.hashCode());
//        System.out.println(lazySingleton03.hashCode());
//        System.out.println(lazySingleton04.hashCode());

        //4.多线程下懒汉式是非线程安全的
//        new Thread(()->{
//            LazySingleton lazySingleton05= LazySingleton.getInstance();
//            System.out.println(lazySingleton05.hashCode());
//        }).start();
//
//        new Thread(()->{
//            LazySingleton lazySingleton05= LazySingleton.getInstance();
//            System.out.println(lazySingleton05.hashCode());
//        }).start();
        //5.多线程下静态内部类单例模式
//        new Thread(() -> {
//            StaticClassSingleton staticClassSingleton1 = StaticClassSingleton.getInstance();
//            System.out.println(staticClassSingleton1);
//        }).start();
//
//        new Thread(() -> {
//            StaticClassSingleton staticClassSingleton1 = StaticClassSingleton.getInstance();
//            System.out.println(staticClassSingleton1);
//        }).start();

        //6.多线程下枚举单例模式
        new Thread(() -> {
            EnumSingleton.Student instance = EnumSingleton.getInstance();
            instance.setAge(3);
            instance.setName("kate");
            System.out.println(instance.toString());
        }).start();

        new Thread(() -> {
            EnumSingleton.Student instance = EnumSingleton.getInstance();
            System.out.println(instance);
        }).start();


    }
}
