package com.kate.base.singleton;

public class StaticClassSingleton {
    //1.构造方法私有
    private StaticClassSingleton(){
        System.out.println("构建对象");
    }
    //2.定义静态内部类
    private static class Instance{
        public static StaticClassSingleton singleton = new StaticClassSingleton();
    }
    //3.公共方法返回类实例
    public static StaticClassSingleton getInstance(){
        return Instance.singleton;
    }
}
