package com.kate.base.ioc;

public class TestObjectFactory {
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        //1.以前：没有IOC之前创建Pig对象
//        RedPig redPig = new RedPig(12, "旺财");
//        BlackPig blackPig = new BlackPig(11, "来福");
        //2.自己写工厂：使用IOC之后创建Pig对象
//        ObjectFactory objectFactory = new ObjectFactory();
//
//        BlackPig blackPig =   objectFactory.getBlackPig();
//        RedPig redPig =  objectFactory.getRegPig();
//        System.out.println(blackPig);
//      	System.out.println(redPig);
    }
}