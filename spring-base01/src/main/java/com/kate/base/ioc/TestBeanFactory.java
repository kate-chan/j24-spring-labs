package com.kate.base.ioc;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Arrays;

public class TestBeanFactory {
    public static void main(String[] args) {
        //1.获取bean工厂方式一：XmlBeanFactory
//        Resource resource = new ClassPathResource("beans.xml");
//        BeanFactory beanFactory = new XmlBeanFactory(resource);//------懒加载
       //1.获取bean工厂方式二：ClassPathXmlApplicationContext  context--->上下文(环境)---及时加载
//        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //1.获取bean工厂方式三：
        ApplicationContext context = new FileSystemXmlApplicationContext("classpath:beans.xml");
        //1.获取bean工厂方式四： web应用，则bean工厂由web容器(tomcat,jetty......)
//        ApplicationContext context = new WebApplicationContext();

        //2.从bean工厂中获取bean

        //2.1.getBean方式：id
//        RedPig rd01 = (RedPig) context.getBean("rd01");
//        System.out.println(rd01);
        //2.2.getBean方式：class  注意：beanFactory中只能存在唯一RedPig的bean，否者报错
//        RedPig rd02 = beanFactory.getBean(RedPig.class);
//        System.out.println(rd02);
        //2.3.getBean方式：class+id
//        RedPig rd02 = context.getBean("rd02", RedPig.class);
//        System.out.println(rd02);
        //2.4.getBean方式：id/class+构造方法参数
//        RedPig rd03 = (RedPig) context.getBean("rd03", 22,"旺财");
//        System.out.println(rd03);
        /**
         * 3个对象之调用了2次构造方法
         *  1次是rd02 bean 调用构造方法
         *  1次是rd01 bean 调用构造方法
         * 结论1：spring bean工厂中默认每个类只会1个实例---------单例模式
         * 结论2：可以通过在beans.xml中定义2个bean实现在bean工厂中产生多个实例
         *
         */
        //补充：可变参数
//        Test01 test01 = new Test01();
//        test01.fun(new int[]{});//必须要参数
//        test01.fun(new int[]{1,2});//必须要参数
//        System.out.println("==========可变参数============");
//        test01.fun2();
//        test01.fun2(1,2);
//        test01.fun2(1,2,3);
//        test01.fun2(1,2,4);
//        test01.fun2(1,2,3,4);
//        test01.fun2(1,2,3,4,5);
    }

}
class Test01 {
    public void fun(int[] args) {
        System.out.println(Arrays.toString(args));
    }

    public void fun2(int... args) {
        System.out.println(Arrays.toString(args));
    }

    //注意：多个参数时，可变参数只能放到最后一个
    public void fun3(String name, int... args) {
        System.out.println(Arrays.toString(args));
    }
}




