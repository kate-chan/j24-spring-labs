package com.kate.base.ioc;

import com.kate.base.pojo.BlackPig;
import com.kate.base.pojo.RedPig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class OtherInstanceTest {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        RedPig rd04 = (RedPig)context.getBean("rd04");
        System.out.println(rd04);

        BlackPig blackPig = (BlackPig) context.getBean("bg01");
        System.out.println(blackPig);

        //销毁工厂
        ((ClassPathXmlApplicationContext)context).close();
    }
}
