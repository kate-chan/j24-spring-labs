package com.kate.base.config;

import com.kate.base.pojo.config.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestJavaConfig {
    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);
        User u1 = (User) context.getBean("u1");
        System.out.println(u1.show());
    }
}
