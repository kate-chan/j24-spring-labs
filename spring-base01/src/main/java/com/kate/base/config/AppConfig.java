package com.kate.base.config;

import com.kate.base.pojo.config.Dog;
import com.kate.base.pojo.config.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * <beans>
 *      <bean id="rd01" class="com.kate.base.pojo.RedPig">
 *         <!--给属性注入值(给属性赋值)-->
 *         <property name="age" value="3"/>
 *         <property name="name" value="kate"/>
 *         <property name="dog" ref="dg01"/>
 *     </bean>
 *
 *     <bean id="dg01" class="com.kate.base.pojo.di.Dog">
 *         <property name="name" value="旺财"/>
 *         <property name="sex" value="母"/>
 *      </bean>
 * </beans>
 *
 * <context:component-scan base-package="com.kate.base.pojo.annotation"/>
 */
@Configuration
@ComponentScan("com.kate.base.pojo.config")//配置注解配置的注解方式进行配置
public class AppConfig {
//    @Bean  //将当前方法的返回值，纳入到ioc容器，bean的默认为：方法名
    @Bean(value = "u1",initMethod = "init",destroyMethod = "destroy")
    @Scope("prototype")
    public User getUser(Dog dog){
        User u1 = new User();
        u1.setAge(3);
        u1.setName("kate");
        u1.setDog(dog);
        return  u1;
    }
//    @Bean
//    public Dog dog(){
//        Dog dog = new Dog();
//        dog.setName("旺财");
//        dog.setSex("公");
//        return dog;
//    }
}
