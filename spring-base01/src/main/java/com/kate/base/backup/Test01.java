package com.kate.base.backup;

import com.kate.base.pojo.BlackPig;
import com.kate.base.pojo.config.Dog;
import com.kate.base.pojo.config.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test01 {
    public static void main(String[] args) {
        //1.非ioc方式,对象需要程序员自己创建，赋值，管理
        BlackPig blackPig = new BlackPig();
        blackPig.setName("kate");
        System.out.println(blackPig);

        User user = new User();

        Dog d1 = new Dog();
        Dog d2 = new Dog();
        //对象属性赋值
        user.setAge(11);
        //对象管理
        user.setDog(d1);
        //2.spring ioc容器(hashmap)完成对象的创建，赋值，管理
        //2.1.获取bean工厂
        ApplicationContext context = new ClassPathXmlApplicationContext("beans2.xml");
        User u1 = (User) context.getBean("user");
        System.out.println(u1);
    }
}
