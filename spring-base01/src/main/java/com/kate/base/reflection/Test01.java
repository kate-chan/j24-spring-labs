package com.kate.base.reflection;

import com.kate.base.pojo.RedPig;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Test01 {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException, NoSuchMethodException, InvocationTargetException {
        /**
         * Class类是描述类的类，这个类的对象包含了描述的类的所有信息(属性，方法，构造方法......)
         *
         * 1.获取Class类对象的3种方式
         *   类名.class
         *   对象名.getClass();
         *   Class.forName("包名.类名")
         * 正向映射：A.java-----编写属性和方法....------A.class-------类加载器--------方法区：构建Class类对象(封装了类的所有信息)
         * 反向映射：方法区：构建Class类对象(封装了类的所有信息)------------A.java这个类所有信息
         *
         * 知识点：类.class与Class.forName的区别？
         * 1.前者是懒加载类，后者是及时加载类
         * */
//        Class clazz01 = RedPig.class;
//        RedPig rd01= (RedPig) clazz01.newInstance();//调用RedPig类的无参数构造方法，构造RedPig类对象


//        RedPig redPig = new RedPig();
//        Class clazz02 = redPig.getClass();


        Class<?> clazz03 = Class.forName("com.kate.base.pojo.RedPig");

        /**
         * 2.获取RedPig类所有属性
         * 描述类-------Class类对象
         * 描述属性-----Field类对象
         */
        //1.返回类的所有的属性，不包括继承过来的属性
        Field[] declaredFields = clazz03.getDeclaredFields();
        //2.返回类的所有公共的属性
        Field[] fields = clazz03.getFields();
        //3.获取某个一个属性
//        Field ageField1 = clazz03.getField("age");
        Field ageField2 = clazz03.getDeclaredField("age");
        /*
         4.给属性赋值和获取属性值
         RedPig rd = new RedPig();
         rd.age=3;报错，私有属性
         rd.setAge(3)
         */
        //1)构建对象
        RedPig redPig = (RedPig) clazz03.newInstance();//RedPig rd = new RedPig();

        //暴力破解封装
        ageField2.setAccessible(true);
        ageField2.set(redPig,3);// rd.age=3;报错，私有属性
        System.out.println(ageField2.get(redPig));//System.out.println(rd.age)

        /***
         * 3.怎么获取RedPig类的所有方法，如何调用方法
         */


        /***
         * 4.如何获取构造方法，如何调用
         *
         * 5.类加载器和双亲委派机制【了解】
         */

    }

}
