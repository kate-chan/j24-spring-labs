package com.kate.base.pojo.config;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <bean id="dg01" class="com.kate.base.pojo.di.Dog">
*         <property name="name" value="旺财"/>
*         <property name="sex" value="母"/>
*      </bean>
 */
@Component
@Data
public class Dog {
    @Value("旺财")
    private String name;
    @Value("公")
    private String sex;
    public Dog() {
        System.out.println("Dog构建对象");
    }
    public Dog(String name, String sex) {
        System.out.println("Dog构建对象");
        this.name = name;
        this.sex= sex;
    }
   	public String toString() {
        return "Dog [name=" + name + ", sex=" + sex + "]";
    }
  	//省略set/get方法
}