package com.kate.base.pojo.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @component相当于在beans-annotation.xml中定义了一个这样的bean
 * <bean id="user" class="com.kate.base.annotation.user">
 *
*       <!--        1.普通属性:基本类型+String-->
 *      <property name="name" value="阳龙哥"/>
 *      <property name="age" value="18"/>
 *      <!--        2.引用类型属性-->
 *      <property name="dog" ref="dg01"/>
 *
 * </bean>
 *
 *
 */
@Component
@Data
public class User {
    //1.普通属性
    @Value("阳哥")
    private String name;
    @Value("18")
    private int age;
    //2.引用属性
    @Resource
    private Dog dog;
    //3.数组
    private String[] arr;
    @Resource
    private Dog[] dogArr;
    //4.集合
    private List<String> list;
    private List<Dog> dogs;
    //5.map集合
    private Map<String, String> map;
    private Map<String, Dog> dogMap;

    public void init(){
        System.out.println("bean开始初始化");
    }

    public void destroy(){
        System.out.println("bean开始销毁");
    }



    public void setArr(String[] arr) {
        this.arr = arr;
    }
    @Autowired
    public void setDog(Dog dog){
        this.dog =dog;
    }


    @Autowired
    public User(Dog dog){
        this.dog = dog;
    }

    public User() {
        System.out.println("User构建对象");
    }

    //构造方法  重载：方法名相同，参数不同(数量，顺序，类型)
    public User(int ag, Dog dg) {
        this.dog = dg;
        this.age = ag;
    }

    public User(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public User(int age, String name) {
        this.age = age;
        this.name = name;
    }


    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + ", dog=" + dog + "]";
    }

    public String show() {
        return "User [name=" + name + ", age=" + age + ",\n dog=" + dog +
                "，\narr=" + Arrays.toString(arr) + "，\ndogArr=" + Arrays.toString(dogArr) + ",\nlist=" + list + ",\ndogs=" + dogs +
                ",\nmap=" + map + ",\ndogMap=" + dogMap + "]";
    }
    //省略set/get方法
}