package com.kate.base.pojo;

public class BluePig {
    private int age;
    private String name;
    public BluePig() {
        System.out.println("调用了BluePig的构造方法");
    }
    public BluePig(int age, String name) {
        this.age = age;
        this.name = name;
    }
  //省略get/set方法
}