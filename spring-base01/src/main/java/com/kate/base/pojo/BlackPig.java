package com.kate.base.pojo;

import lombok.Data;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

@Data
public class BlackPig implements InitializingBean, DisposableBean {
    private int age;
    Integer a;
    private String name;
    public BlackPig() {
        System.out.println("调用了BlackPig的构造方法");
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    public BlackPig(int age, String name) {
        this.age = age;
        this.name = name;
    }
    //省略get/set方法
    // 生命周期初始化方法
    @Override
    public void afterPropertiesSet() {
        System.out.println("开始对BlackPigr类对象，进行业务初始化了.......");
    }

    // 生命周期销毁方法
    @Override
    public void destroy() {
        System.out.println("开始对BlackPig类对象，进行销毁前的操作了.......");
    }
}
