package com.kate.base.pojo.di;

import lombok.Data;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Data
public class User {
    //1.普通属性
    private String name;
    private int age;
    //2.引用属性
    private Dog dog;
    //3.数组
    private String[] arr;
    private Dog[] dogArr;
    //4.集合
    private List<String> list;
    private List<Dog> dogs;
    //5.map集合
    private Map<String, String> map;
    private Map<String, Dog> dogMap;

    public void setArr(String[] arr){
        this.arr = arr;
    }


    public User() {
        System.out.println("User构建对象");
    }

    //构造方法  重载：方法名相同，参数不同(数量，顺序，类型)
    public User(int ag, Dog dg) {

        this.dog = dg;
        this.age = ag;
    }
    public User(String name,int age) {
        this.age = age;
        this.name = name;
    }
    public User(int age, String name) {
        this.age = age;
        this.name = name;
    }



    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + ", dog=" + dog + "]";
    }

    public String show() {
        return "User [name=" + name + ", age=" + age + ",\n dog=" + dog +
                "，\narr=" + Arrays.toString(arr) + "，\ndogArr=" + Arrays.toString(dogArr) +",\nlist=" + list + ",\ndogs=" + dogs +
                ",\nmap=" + map + ",\ndogMap=" + dogMap + "]";
    }
    //省略set/get方法
}