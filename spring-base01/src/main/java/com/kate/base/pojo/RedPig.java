package com.kate.base.pojo;

import lombok.Data;

@Data
public class RedPig {

    public void init(){
        System.out.println("对象开始初始化，一般初始化一些，对象需要的资源，比如IO,数据库链接等等.....");
    }
    public void destroy(){
        System.out.println("对象销毁时回调，一般资源释放，比如IO,数据库链接等等.....");
    }




    static {//静态块：再RedPig类加载到内存中(方法区)，就会被执行，且仅仅被执行一次
        System.out.println("RedPig类已经加载类..........");
    }
    private int age;
    private String name;
    public RedPig() {
       System.out.println("调用了RedPig的构造方法");
    }
   	public RedPig(int  age, String name) {
        System.out.println("调用有参数的构造方法构造对象");
        this.age = age;
        this.name = name;
    }
  	//省略get/set方法
    public int hashCode(){
       return super.hashCode();
    }
}