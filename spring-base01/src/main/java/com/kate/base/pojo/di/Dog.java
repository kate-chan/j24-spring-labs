package com.kate.base.pojo.di;

import lombok.Data;

@Data
public class Dog {
    private String name;
    private String sex;
    public Dog() {
        System.out.println("Dog构建对象");
    }
    public Dog(String name, String sex) {
        System.out.println("Dog构建对象");
        this.name = name;
        this.sex= sex;
    }
   	public String toString() {
        return "Dog [name=" + name + ", sex=" + sex + "]";
    }
  	//省略set/get方法
}