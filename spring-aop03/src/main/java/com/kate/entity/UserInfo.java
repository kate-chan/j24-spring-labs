package com.kate.entity;

import lombok.Data;

@Data
public class UserInfo {
    private Integer id;
    private String username;
    private Integer money;
}
