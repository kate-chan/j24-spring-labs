package com.kate.aspectj;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class XmxMallAop {
    //1.切点-----那个方法
    @Pointcut("execution(public int com.kate.aop.dao.impl.UserDaoImpl.add(..))")
    public void pointCut(){}
    //2.增强-----植入方法的内容
    //2.1.前置增强
    @Before("pointCut()")
    public void beforeEnhancer(JoinPoint joinPoint){
        System.out.println(joinPoint);
        Object[] args = joinPoint.getArgs();
        System.out.println("【前置通知】......拦截参数："+Arrays.toString(args));
        System.out.println("【前置通知】.............执行鉴权......");
    }
    @AfterReturning(value = "execution(public int com.kate.aop.dao.impl.UserDaoImpl.add(..))",returning = "result")
    public void AfterReturning(JoinPoint joinPoint ,Object result){
        System.out.println("获取方法执行结果："+result);
        System.out.println("【后置通知】.......执行日志流水......");
    }
    @Around("pointCut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("【环绕通知】......开始增强");
        //调用目标方法
        Object result = joinPoint.proceed();

        System.out.println("【环绕通知】......结束增强");
        return result;
    }

    @AfterThrowing("pointCut()")
    public void afterThrowing(JoinPoint joinPoint){
        System.out.println("【异常通知】.....执行了.... :切点："+joinPoint.getSignature()+"\t参数为："+joinPoint.getArgs());
    }

    @After("pointCut()")
    public void after(JoinPoint joinPoint){
        System.out.println("【最终通知】......执行了....");
    }

}
