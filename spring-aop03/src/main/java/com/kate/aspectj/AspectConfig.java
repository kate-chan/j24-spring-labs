package com.kate.aspectj;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)  //1.开启aspectj功能  2.强制spring使用cglib ,proxyTargetClass = true
@ComponentScan({"com.kate.aspectj","com.kate.aop.dao.impl"})
public class AspectConfig {
}
