package com.kate.service.impl;

import com.kate.entity.UserInfo;
import com.kate.exception.TransferAccountException;
import com.kate.mapper.UserInfoMapper;
import com.kate.service.IUserInfoService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.io.FileNotFoundException;

/**
 * 业务层 ===声明式式事务
 *
 * 事务：一列操作要么同时成功，要么同时失败！！ 目标：保证数据的一致性
 * 事务特性：
 * A:原子性      要么同时成功，要么同时失败
 * C:一致性性    事务前与事务后 数据一性性
 * I: 隔离性     事务与事务之间存在隔离
 * D: 持久性     事务完成后，数据存储到磁盘，不存在数据丢失
 */

@Component
public class UserInfoServiceImpl3 implements IUserInfoService {
    @Resource
    private UserInfoMapper userInfoMapper;

    @Resource
    private TransactionTemplate transactionTemplate;


    //内部类
    private class Result{
        boolean resultValue =false;
    }
    @Override
    public boolean zz(int from, int to, int money) throws FileNotFoundException {
//        final boolean result = true;
        Result result = new Result();
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {

            protected void doInTransactionWithoutResult(TransactionStatus status) {
                try {
                    result.resultValue  =  zzOptional(from,to,money);
                } catch (TransferAccountException ex) {
                    System.out.println("事务回滚");
                    status.setRollbackOnly();
                }
            }
        });
        return result.resultValue;
    }
    private boolean zzOptional(int from, int to, int money)  {
        // 一,==============旺财的钱减少200==============
        // 1.1查询旺财有多少钱
        UserInfo wc = userInfoMapper.selectByPrimaryKey(from);
        System.out.println(wc);
        // 1.2扣旺财有300
        int result = 0;
        if(wc!=null&&wc.getMoney()>money) {
            wc.setMoney(wc.getMoney() - money);
            result = userInfoMapper.updateByPrimaryKey(wc);
        }
        // ==============二,来福的钱增加200==============
        // 1.1查询旺财有多少钱
        UserInfo lf = userInfoMapper.selectByPrimaryKey(to);
        System.out.println(lf);
        // 1.2扣旺财有300
        int result2 = 0;
        if (lf != null) {
            lf.setMoney(lf.getMoney() + money);
            result2 = userInfoMapper.updateByPrimaryKey(lf);
        }
        // ==============最后的结果==============
        if (result <= 0 || result2 <= 0) {
            throw new TransferAccountException("转账失败");
        }
        return true;
    }


}