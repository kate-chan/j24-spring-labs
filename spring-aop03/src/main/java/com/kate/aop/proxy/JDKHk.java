package com.kate.aop.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 医生-----证件(代表你具有这个资历)
 * 接口-----一组功能的约定
 */
public class JDKHk  implements InvocationHandler {
    //1.目标
    private Object target;

    public JDKHk(Object target) {
        this.target = target;
    }


    /**
     *
     * @param proxy     代理对象
     * @param method    目标对象的目标方法
     * @param args      传入给目标方法的参数
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("开始入侵");

        //执行目标方法 userDao.add(3,5)
        Object result = method.invoke(target, args);

        System.out.println("结束入侵");
        return result;
    }
}
