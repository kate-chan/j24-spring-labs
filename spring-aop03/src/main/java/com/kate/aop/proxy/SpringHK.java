package com.kate.aop.proxy;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

/**
 * 医生-----证件(代表你具有这个资历)
 * 接口-----一组功能的约定
 *
 * EnCha
 */
public class SpringHK implements MethodInterceptor {
    /**
     * @param methodInvocation   目标方法
     * @return
     * @throws Throwable
     */
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        System.out.println("开始入侵---鉴权");
        //执行目标方法
        Object result = methodInvocation.proceed();

        System.out.println("结束入侵----日志");
        return result;
    }
}
