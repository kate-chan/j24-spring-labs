package com.kate.aop.test;

import com.kate.aop.dao.UserDao;
import com.kate.aop.dao.impl.UserDaoImpl;
import com.kate.aop.proxy.JDKHk;

import java.lang.reflect.Proxy;

public class JdkTest {
    public static void main(String[] args) {

        //将生成的代理类Proxy0文件保存
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
        //不用代理的情况
//        UserDao userDao = new UserDaoImpl();
//        int result = userDao.add(3, 5);
//        System.out.println("result="+result);

        //jdk黑客---jdk动态代理
        //1.目标对象
        UserDao userDao = new UserDaoImpl();
        //2.黑客对象
        JDKHk jdkHk = new JDKHk(userDao);
        //3.代理对象(与目标对象非常相似，是黑客对象和目标对象的柔和体)
        UserDao userDao2 = (UserDao) Proxy.newProxyInstance(
                ClassLoader.getSystemClassLoader(),
                new Class[]{UserDao.class},
                jdkHk);
        //4.调用目标方法
        int result = userDao2.add(3, 5);
        System.out.println(result);

    }
}
