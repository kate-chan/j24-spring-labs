package com.kate.exception;

/**
 *
 * 转账异常
 * 为什么要继承RuntimeException？为什么不继承Exception?
 * 答：spring 默认只会对运行时异常进行回滚！！
 *
 */
public class TransferAccountException extends RuntimeException{
    public TransferAccountException(String message) {
        super(message);
    }
}
