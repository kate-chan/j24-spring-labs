package com.kate.mapper;

import com.kate.entity.UserInfo;

public interface UserInfoMapper {
    UserInfo selectByPrimaryKey(int from);

    int updateByPrimaryKey(UserInfo userInfo);
}
