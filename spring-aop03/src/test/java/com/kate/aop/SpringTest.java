package com.kate.aop;


import com.kate.aop.dao.impl.UserDaoImpl2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@ContextConfiguration("classpath:beans.xml")
public class SpringTest {

//    @Resource(name = "userDaoProxy")   //代理对象为由接口的目标
//    private UserDao userDao;

    @Resource(name = "userDaoProxy")
    private UserDaoImpl2 userDao;
    @Test
    public void TestSpringAop(){
        int result = userDao.add(3, 5);
        System.out.println(result);
    }

}
