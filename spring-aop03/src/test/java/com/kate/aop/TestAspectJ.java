package com.kate.aop;

import com.kate.aop.dao.UserDao;
import com.kate.aspectj.AspectConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AspectConfig.class)
public class TestAspectJ {
    @Resource
    private UserDao userDao;

    @Test
    public void testUserDao(){
        int result = userDao.add(3, 5);
        System.out.println(result);
    }
}
