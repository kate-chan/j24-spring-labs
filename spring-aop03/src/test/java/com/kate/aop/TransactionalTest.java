package com.kate.aop;

import com.kate.service.IUserInfoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.FileNotFoundException;

@RunWith(SpringRunner.class)
//@ContextConfiguration("classpath:beans-tx.xml")  //編程式事務配置
//@ContextConfiguration("classpath:beans-declare.xml")//声明式事務配置
@ContextConfiguration("classpath:beans-anno.xml")//注解事务
public class TransactionalTest {

    @Resource
    private IUserInfoService iUserInfoService;

    //////////////////////編程式事務/////////////////
    @Test
    public void testTx() throws FileNotFoundException {
        boolean result = iUserInfoService.zz(1, 581, 200);
        System.out.println(result?"轉賬成功":"转账失败");
    }
    //////////////////////声明式事務/////////////////
    @Test
    public void testDeclaredTx() throws FileNotFoundException {
        boolean result = iUserInfoService.zz(1, 581333, 200);
        System.out.println(result?"轉賬成功":"转账失败");
    }

    //////////////////////注解式事務/////////////////
    @Test
    public void testDeclaredAnno() throws FileNotFoundException {
        boolean result = iUserInfoService.zz(1, 5812222, 200);
        System.out.println(result?"轉賬成功":"转账失败");
    }

}
